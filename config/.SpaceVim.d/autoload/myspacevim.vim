function! myspacevim#before() abort
let g:vimfiler_ignore_pattern = '^\%(\.git\|\.DS_Store\)$'

let profile = SpaceVim#mapping#search#getprofile('rg')
let default_opt = profile.default_opts + ['--no-ignore-vcs']
call SpaceVim#mapping#search#profile({'rg' : {'default_opts' : default_opt}})
endfunction

function! myspacevim#after() abort
endfunction

#!/bin/bash

ANSIBLE_STDOUT_CALLBACK=debug ansible-playbook --connection=local --inventory 127.0.0.1, playbook.yml --ask-become-pass -v
